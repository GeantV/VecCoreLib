#ifndef VECCORELIB_MATH_H
#define VECCORELIB_MATH_H

#include "Math/FastExp.h"
#include "Math/FastLog.h"
#include "Math/FastPow.h"
#include "Math/FastSinCos.h"

#endif // VECCORELIB_MATH_H
